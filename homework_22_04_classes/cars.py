from datetime import datetime


class Car:
    def __init__(self, brand, color, year):
        self.brand = brand
        self.color = color
        self.year = year
        self.__mileage = 0

    def drive(self, distance):
        self.__mileage += distance

    def get_age(self):
        return datetime.today().year - self.year

    def get_mileage(self):
        return self.__mileage

    def repaint(self, color):
        self.color = color

    def __str__(self):
        return f"Car {self.brand} of color {self.color} from {self.year} year with mileage {self.__mileage}"


if __name__ == '__main__':
    car1 = Car('Mazda', 'Silver', 2017)
    car2 = Car('Opel', 'White', 2019)
    car3 = Car('Citroen', 'Red', 2020)

    print(car1)
    car1.drive(1000)
    print(car1)
    print(car1.get_age())
    car1.repaint('Black')
    print(car1)
