from assertpy import assert_that

from homework_22_04_classes.cars import Car

car = Car('Mazda', 'white', 2019)


def test_new_car_is_of_type_car():
    assert_that(car).is_type_of(Car)


def test_new_car_is_instance_of_object():
    assert_that(car).is_instance_of(object)


def test_new_car_string():
    input_car_str = 'Car Mazda of color white from 2019 year with mileage 0'
    assert_that(str(car)).is_equal_to(input_car_str)


def test_new_car_brand_is_mazda():
    brand = 'Mazda'
    assert_that(car.brand).is_equal_to(brand)


def test_new_car_color_is_white():
    color = 'white'
    assert_that(car.color).is_equal_to(color)


def test_drive_1000_mile():
    car.drive(1000)
    car.drive(2000)
    assert_that(car.get_mileage()).is_equal_to(3000)


def test_repaint_silver_color():
    car.repaint('silver')
    assert_that(car.color).is_equal_to('silver')


def test_get_age_of_a_car():
    assert_that(car.get_age()).is_equal_to(5)
