from exercise_1 import is_car_has_guaranty


def test_car_year_2020_mileage_30000_has_guaranty():
    assert is_car_has_guaranty(2020, 30000) is True


def test_car_year_2020_mileage_70000_has_guaranty():
    assert is_car_has_guaranty(2020, 70000) is False


def test_car_year_2016_mileage_30000_has_guaranty():
    assert is_car_has_guaranty(2016, 30000) is False


def test_car_year_2016_mileage_120000_has_guaranty():
    assert is_car_has_guaranty(2016, 120000) is False
