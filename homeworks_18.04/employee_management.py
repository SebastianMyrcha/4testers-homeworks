import random

FEMALE_NAMES = ['Urszula', 'Marta', 'Zofia', 'Iza', 'Agata', 'Beata']
MALE_NAMES = ['Jan', 'Piotr', 'Tomek', 'Pawel', 'Marcin', 'Sylwester', 'Jakub']
SURNAMES = ['Nowak', 'Kowal', 'Mlynarz', 'Lis', 'Gruszka']
DOMAINS = ['onet.pl', 'gmail.com', 'o2.pl', 'wp.pl']


def generate_random_email(is_female):
    names = FEMALE_NAMES if is_female else MALE_NAMES
    return f"{random.choice(names)}.{random.choice(SURNAMES)}@{random.choice(DOMAINS)}"


def generate_employee_dict():
    is_female = bool(random.randint(0, 1))
    email = generate_random_email(is_female)
    seniority_years = random.randint(1, 40)
    return {
        "email": email,
        "seniority_years": seniority_years,
        "female": is_female
    }


def generate_list_of_employee_dict(list_length):
    return [generate_employee_dict() for x in range(list_length)]


def get_emails_with_seniority_years_greater_than_10(employees):
    return [employee['email'] for employee in employees if employee['seniority_years'] > 10]


def get_female_emails(employees):
    return [employee['email'] for employee in employees if employee['female']]


if __name__ == '__main__':
    print(generate_employee_dict())
    employees = generate_list_of_employee_dict(10)
    print(employees)
    print(get_female_emails(employees))
    print(get_emails_with_seniority_years_greater_than_10(employees))
