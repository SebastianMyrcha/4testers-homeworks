from datetime import datetime
def is_car_has_guaranty(year, mileage):
    today = datetime.today()
    current_year = today.year
    return current_year - year < 5 and mileage < 60000

if __name__ == '__main__':
    print(is_car_has_guaranty(2016, 30000))