from employee_management import *


def test_generate_random_email():
    assert "@" in generate_random_email(is_female=True)


def test_employee_dict_is_dict():
    assert type(generate_employee_dict()) is dict


def test_employee_dict_has_correct_keys():
    employee_dict = generate_employee_dict()
    assert "email" in employee_dict and "seniority_years" in employee_dict and "female" in employee_dict


def test_employee_seniority_years_is_between_1_and_40():
    assert 0 < generate_employee_dict()['seniority_years'] < 40


def test_employee_email_is_correct_email():
    employee_dict = generate_employee_dict()
    assert type(employee_dict['email']) is str and "@" in employee_dict['email']


def test_employee_female_is_type_of_bool():
    assert type(generate_employee_dict()['female']) is bool


def test_list_of_employees_has_correct_length():
    assert len(generate_list_of_employee_dict(10)) == 10


def test_all_employees_has_seniority_years_greater_than_10():
    employees_list = [{'email': 'Agata.Gruszka@o2.pl', 'seniority_years': 22, 'female': True}]
    assert len(get_emails_with_seniority_years_greater_than_10(employees_list)) is 1


def test_no_employees_has_seniority_years_greater_than_10():
    employees_list = [{'email': 'Agata.Gruszka@o2.pl', 'seniority_years': 9, 'female': True}]
    assert len(get_emails_with_seniority_years_greater_than_10(employees_list)) is 0


def test_all_employees_are_female():
    employees_list = [{'email': 'Agata.Gruszka@o2.pl', 'seniority_years': 22, 'female': True}]
    assert len(get_female_emails(employees_list)) is 1


def test_no_employees_are_female():
    employees_list = [{'email': 'Piotr.Nowak@o2.pl', 'seniority_years': 25, 'female': False}]
    assert len(get_female_emails(employees_list)) is 0
