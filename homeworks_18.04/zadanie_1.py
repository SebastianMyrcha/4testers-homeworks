import random
from datetime import datetime

# Listy zawierające dane do losowania
female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']


def get_random_firstname(is_female):
    return random.choice(female_fnames) if is_female else random.choice(male_fnames)


def get_random_surname():
    return random.choice(surnames)


def get_random_country():
    return random.choice(countries)


def get_random_age():
    return random.randint(5, 45)


def is_adult(age):
    return age >= 18


def calculate_birth_year(age):
    current_year = datetime.today().year
    return current_year - age


def generate_random_email(name, surname):
    return f"{name}.{surname}@example.com"


def generate_person_dict(is_female):
    firstname = get_random_firstname(is_female)
    lastname = get_random_surname()
    email = generate_random_email(firstname, lastname)
    age = get_random_age()
    country = get_random_country()

    return {
        'firstname': firstname,
        'lastname': lastname,
        'email': email,
        'age': age,
        'country': country,
        'adult': is_adult(age),
        'birth_year': calculate_birth_year(age)
    }


def generate_list_of_10_person_dict():
    return [generate_person_dict(n % 2) for n in range(10)]


def describe_person(person):
    return (f"Hi! I'm {person['firstname']} {person['lastname']}. "
            f"I come from {person['country']} and I was born in {person['birth_year']}.")


if __name__ == '__main__':
    print(get_random_firstname(is_female=True))
    print(generate_person_dict(is_female=False))
    persons = generate_list_of_10_person_dict()
    for person in persons:
        print(describe_person(person))
